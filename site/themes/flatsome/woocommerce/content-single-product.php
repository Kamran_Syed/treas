<?php

    /**
     * The template for displaying lookbook product style content within loops.
     *
     * Override this template by copying it to yourtheme/woocommerce/content-product.php
     *
     * @author      WooThemes
     * @package     WooCommerce/Templates
     * @version     1.6.4
     */

 
    global $post, $product, $flatsome_opt;

    // Get category permalink
    $permalinks     = get_option( 'woocommerce_permalinks' );
    $category_slug  = empty( $permalinks['category_base'] ) ? _x( 'product-category', 'slug', 'woocommerce' ) : $permalinks['category_base'];
 
?>

<div itemscope itemtype="http://schema.org/Product" id="product-<?php the_ID(); ?>" <?php post_class(); ?>> 
    
<div class="row">    
        <?php
            /**
             * woocommerce_before_single_product hook
             *
             * @hooked woocommerce_show_messages - 10
             */
             do_action( 'woocommerce_before_single_product' );
        ?>    
        <div class="large-6 columns product-gallery aspk_product_border">        
        
            <?php
                /**
                 * woocommerce_show_product_images hook
                 *
                 * @hooked woocommerce_show_product_sale_flash - 10
                 * @hooked woocommerce_show_product_images - 20
                 */
                do_action( 'woocommerce_before_single_product_summary' );
            ?>
        
        </div><!-- end large-6 - product-gallery -->
        
        <div class="product-info large-4 small-12 columns left">
                <?php
                    /**
                     * woocommerce_single_product_summary hook
                     *
                     * @hooked woocommerce_template_single_title - 5
                     * @hooked woocommerce_template_single_price - 10
                     * @hooked ProductShowReviews() (inc/template-tags.php) - 15
                     * @hooked woocommerce_template_single_excerpt - 20
                     * @hooked woocommerce_template_single_add_to_cart - 30
                     * @hooked woocommerce_template_single_meta - 40
                     * @hooked woocommerce_template_single_sharing - 50
                     */
                    do_action( 'woocommerce_single_product_summary' );
                ?>
			<div class="aspk_product_details">
				<span class="aspk_products_span">Seller</span>
			</div>
			<div class="aspk_product_details">
				<span class="aspk_products_span">location</span>
				<span>
				<?php
				$city = get_post_meta( $product->id, '_aspk_city', true );
				$state = get_post_meta( $product->id, '_aspk_country', true );
				if(! empty($city) || ! empty($state) ) { 
					echo $city.','.$state;
				}
				?>
				</span>
			</div>
			<div style="padding-bottom:0.5em;font-size:24px;border-bottom:1px solid #ebebeb;margin-top: 1.4em;">Details</div>
			<div class="aspk_product_details" style="margin-top: 1em;">
				<span class="aspk_products_span">Dimensions</span>
				<span>
					<?php 
				$width = get_post_meta( $product->id, '_width', true );
				$height = get_post_meta( $product->id, '_height', true );
				$length = get_post_meta( $product->id, '_length', true );
				if(! empty($width) || ! empty($height) || ! empty($length) ) { 
				?>
				<?php echo $width .'″W x '.$length.'″D x '.$height.'″H'; 
				}
				?>
				</span>
			</div>
			<div class="aspk_product_details">
				<span class="aspk_products_span">Condition</span>
				<span>
				<?php 
					$conditions = get_post_meta( $product->id, '_aspk_condition', true );
					echo $conditions;
				?>
				</span>
			</div>
			<div class="aspk_product_details">
				<span class="aspk_products_span">Style</span>
			</div>
			<div>
				<?php require_once('single-product/tabs/description-product.php'); ?>
			</div>
        
        </div><!-- end product-info large-4 -->

<div class="product-page-aside large-2 small-12 columns text-center hide-for-small">
    
    <div class="next-prev-nav">
        <?php // edit this in inc/template-tags.php // ?>
        <?php next_post_link_product('%link', 'icon-angle-left next', true); ?>
        <?php previous_post_link_product('%link', 'icon-angle-right prev', true); ?>
    </div>

     <?php  woocommerce_get_template('single-product/up-sells.php');?> 

</div><!-- .product-page-aside -->
     
        
</div><!-- end row -->
    
    
<?php
    //Get the Thumbnail URL for pintrest
    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' );
?>


    
<!--<div class="row">
    <div class="large-12 columns">
        <div class="product-details <?php echo $flatsome_opt['product_display']; ?>-style">
               <div class="row">

                    <div class="large-12 columns ">
                    <?php //woocommerce_get_template('single-product/tabs/tabs.php'); ?>
                    </div>
                
               </div>
        </div>

        <hr/>
    </div> 
</div><!-- .row -->


    <div class="related-product">
        <?php
            /**
             * woocommerce_after_single_product_summary hook
             *
             * @hooked woocommerce_output_related_products - 20
             */

            do_action( 'woocommerce_after_single_product_summary' );

        ?>
    </div><!-- related products -->

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>