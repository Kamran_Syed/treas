<?php
global $woo_options;
global $woocommerce;
global $flatsome_opt;
?>
<!DOCTYPE html>
<!--[if lte IE 9 ]><html class="ie lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/agile-bootstrap.css">
	<!-- Custom favicon-->
	<link rel="shortcut icon" href="<?php if ($flatsome_opt['site_favicon']) { echo $flatsome_opt['site_favicon']; ?>
	<?php } else { ?><?php echo get_template_directory_uri(); ?>/favicon.png<?php } ?>" />

	<!-- Retina/iOS favicon -->
	<link rel="apple-touch-icon-precomposed" href="<?php if ($flatsome_opt['site_favicon_large']) { echo $flatsome_opt['site_favicon_large']; ?>
	<?php } else { ?><?php echo get_template_directory_uri(); ?>/apple-touch-icon-precomposed.png<?php } ?>" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php 
// HTML Homepage Before Header // Set in Theme Option > HTML Blocks
if($flatsome_opt['html_intro'] && is_front_page()) echo '<div class="home-intro">'.do_shortcode($flatsome_opt['html_intro']).'</div>' ?>

	<div id="wrapper">
		<div class="header-wrapper">
		<?php do_action( 'before' ); ?>
		<?php if(!isset($flatsome_opt['topbar_show']) || $flatsome_opt['topbar_show']){ ?>
		<div id="top-bar">
			<div class="row">
				<div class="large-12 columns">
					<!-- left text -->
					<div class="left-text left">
					 <?php if ( has_nav_menu( 'top_bar_nav2' ) ) {
							wp_nav_menu(array(
								'theme_location' => 'top_bar_nav2',
								'menu_class' => 'top-bar-nav',
								'before' => '',
								'after' => '',
								'link_before' => '',
								'link_after' => '',
								'depth' => 2,
								'fallback_cb' => false,
								'walker' => new FlatsomeNavDropdown
							));
						}
						?>
					</div>
					<!-- right text -->
					<div class="right-text right">
						 <?php if ( has_nav_menu( 'top_bar_nav' ) ) : ?>
						<?php  
								wp_nav_menu(array(
									'theme_location' => 'top_bar_nav',
									'menu_class' => 'top-bar-nav',
									'before' => '',
									'after' => '',
									'link_before' => '',
									'link_after' => '',
									'depth' => 2,
									'fallback_cb' => false,
									'walker' => new FlatsomeNavDropdown
								));
						?>
						 <?php else: ?>
                            Define your top bar navigation in <b>Apperance > Menus</b>
                        <?php endif; ?>
					</div><!-- .pos-text -->

				</div><!-- .large-12 columns -->
			</div><!-- .row -->
		</div><!-- .#top-bar -->
		<?php }?>

		<div class="row">
			<div class="large-4 columns">&nbsp;&nbsp;&nbsp;</div>
			<div class="large-4 columns">
				<a  id="aspk_logo" href="<?php echo home_url(); ?>">
					<img src="<?php echo get_bloginfo('template_url') ?>/images/images.jpg"/>
				</a>
			</div>
			<div class="large-4 columns">&nbsp;&nbsp;&nbsp;</div>
		</div>
		<div class="sticky-wrapper">
			<div class="row">
				<div class="large-10 columns">&nbsp;&nbsp;&nbsp;</div>
				<div class="large-2 columns">
					<?php if (!isset($flatsome_opt['search_pos']) || $flatsome_opt['search_pos'] == 'left') { ?>
						<li class="search-dropdown">
							<a href="#" class="nav-top-link icon-search" onClick="return false;"></a>
							<div class="nav-dropdown">
								<?php if(function_exists('get_product_search_form')) {?><?php 
									get_product_search_form();
								} //else {
								//	get_search_form();
								//} ?>	
							</div><!-- .nav-dropdown -->
						</li><!-- .search-dropdown -->
						<?php } ?>
				</div>
			</div>
		<header id="masthead" class="site-header" role="banner">
			<div class="row"> 
				<div class="large-12 columns">
					<?php if(!isset($flatsome_opt['nav_position']) || $flatsome_opt['nav_position'] == 'top'){ ?>
						<ul id="site-navigation" class="header-nav">
							<?php if ( has_nav_menu( 'primary' ) ) : ?>

								<?php  
								wp_nav_menu(array(
									'theme_location' => 'primary',
									'container'       => false,
									'items_wrap'      => '%3$s',
									'depth'           => 0,
									'walker'          => new FlatsomeNavDropdown
								));
							?>

							<?php else: ?>
								<li>Define your main navigation in <b>Apperance > Menus</b></li>
							<?php endif; ?>								
						</ul>
					<?php } ?>
				</div>
			</div><!-- .row -->
</header><!-- .header -->
</div><!-- .sticky-wrapper -->
</div><!-- .header-wrapper -->


<?php if(isset($flatsome_opt['html_after_header'])){
	// AFTER HEADER HTML BLOCK
	echo do_shortcode($flatsome_opt['html_after_header']);
} ?>

<div id="main-content" class="site-main <?php echo $flatsome_opt['content_color']; ?>">
<?php 
//adds a border line if header is white
if (strpos($flatsome_opt['header_bg'],'#fff') !== false || $flatsome_opt['nav_position'] == 'top') {
		  echo '<div class="row"><div class="large-12 columns"><div class="top-divider"></div></div></div>';
} ?>

<!-- woocommerce message -->
<?php  if(function_exists('wc_print_notices')) {wc_print_notices();}?>