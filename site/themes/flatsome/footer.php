<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</div><!-- #main-content -->


<footer class="footer-wrapper" role="contentinfo">	
<?php if(isset($flatsome_opt['html_before_footer'])){
	// BEFORE FOOTER HTML BLOCK
	echo do_shortcode($flatsome_opt['html_before_footer']);
} ?>


<!-- FOOTER 1 -->
<?php if ( is_active_sidebar( 'sidebar-footer-1' ) ) : ?>
<div class="footer footer-1 <?php echo $flatsome_opt['footer_1_color']; ?>"  style="background-color:<?php echo $flatsome_opt['footer_1_bg_color']; ?>">
	<div class="row">
   		<?php dynamic_sidebar('sidebar-footer-1'); ?>        
	</div><!-- end row -->
</div><!-- end footer 1 -->
<?php endif; ?>


<!-- FOOTER 2 -->
<?php if ( is_active_sidebar( 'sidebar-footer-2' ) ) : ?>
<div class="footer footer-2 <?php echo $flatsome_opt['footer_2_color']; ?>" style="background-color:<?php echo $flatsome_opt['footer_2_bg_color']; ?>">
	<div class="row">

   		<?php dynamic_sidebar('sidebar-footer-2'); ?>        
	</div><!-- end row -->
</div><!-- end footer 2 -->
<?php endif; ?>


<?php if(isset($flatsome_opt['html_after_footer'])){
	// AFTER FOOTER HTML BLOCK
	echo do_shortcode($flatsome_opt['html_after_footer']);
} ?>


<div class="absolute-footer <?php echo $flatsome_opt['footer_bottom_style']; ?>" style="background-color:<?php echo $flatsome_opt['footer_bottom_color']; ?>">
<div class="row">
	<div class="large-2 columns">
		<h4>NEED HELP?</h4>
		<?php
		if ( has_nav_menu( 'footer-menu-info' ) ) { wp_nav_menu( array('theme_location' => 'footer-menu-info', 'menu_class' => 'footer-nav' )); 
			} 
		?>

	</div><!-- .large-2 -->
	<div class="large-2 columns">
		<h4>MY ACCOUNT</h4>
		<?php
		if ( has_nav_menu( 'footer' ) ) { wp_nav_menu( array('theme_location' => 'footer', 'menu_class' => 'footer-nav' )); 
			} 
		?>

	</div><!-- .large-2 -->
	<div class="large-2 columns">
		<h4>OUR COMPANY</h4>
		<?php
		if ( has_nav_menu( 'footer_company' ) ) { wp_nav_menu( array('theme_location' => 'footer_company', 'menu_class' => 'footer-nav' )); 
			} 
		?>

	</div><!-- .large-2 -->
	<div class="large-3 columns">
		<h4>STAY CONNECTED</h4>
		<?php
		if ( has_nav_menu( 'footer_connected' ) ) { wp_nav_menu( array('theme_location' => 'footer_connected', 'menu_class' => 'footer-nav' )); 
			} 
		?>

	</div><!-- .large-2 -->
	<div class="large-3 columns">
		<h4>CHAIRISH FOR IPHONE</h4>
		<?php
		if ( has_nav_menu( 'footer_iphone' ) ) { wp_nav_menu( array('theme_location' => 'footer_iphone', 'menu_class' => 'footer-nav' )); 
			} 
		?>

	</div><!-- .large-2 -->
</div><!-- .row-->
</div><!-- .absolute-footer -->
</footer><!-- .footer-wrapper -->
</div><!-- #wrapper -->

<!-- back to top -->
<a href="#top" id="top-link"><span class="icon-angle-up"></span></a>
<div class="scroll-to-bullets"></div>

<?php if(isset($flatsome_opt['html_scripts_footer'])){
	// Insert footer scripts
	echo $flatsome_opt['html_scripts_footer'];
} ?>


<?php wp_footer(); ?>

</body>
</html>