<?php
/*
Plugin Name: Treas
Plugin URI: 
Description: Treas project
Version: 1.2
Author URI: http://agilesolutionspk.com
*/
if ( !class_exists( 'Agile_treas' )){

	class Agile_treas{
		
		function __construct(){
			
			add_shortcode('seller_treas', array(&$this, 'seller_form'));
			register_activation_hook( __FILE__, array($this, 'install'));
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));
			add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));
			add_action('wp_footer', array(&$this,'front_end' ));
			add_action( 'wp_ajax_aspk_jq_uploader',  array(&$this,'handle_image_upload' )); 
			add_action( 'wp_ajax_nopriv_aspk_jq_uploader',  array(&$this,'handle_image_upload' ));	
			add_shortcode('aspk_product_catg', array(&$this,'product_catg'));
			add_action( 'wp_ajax_show_sub_category', array(&$this,'ajax_sub_category' ));
		}
		
		function product_catg(){
			if(is_page('collection')){ 
			If(isset($_GET['catid'])) $category_slug = $_GET['catid'];
				return do_shortcode( '[product_category category="'.$category_slug.'"]' ); 
			}else{
				echo "Products not Available";
			}
		}
		
		function ajax_sub_category(){
			$cat_id = $_POST['main_categories'];
			$args = array(
				   'hierarchical' => 1,
				   'show_option_none' => '',
				   'hide_empty' => 0,
				   'parent' => $cat_id,
				   'taxonomy' => 'product_cat'
				);
			  $subcats = get_categories($args);
			if(empty($subcats)){
				$rat = array('st' => 'error','data'=>'','msg'=>'not found');
				echo json_encode($rat);	
			}else{
			 ob_start();
			  ?>
			  
			  <div class="row" style="margin-bottom: 1em;">
					<div class="col-md-12 sub_category" style="margin-top: 1em;">Sub Category *</div>
				</div>
			   <select style="border: 1px solid #e3e3e3;border-radius: 1px;background-color: #f5f5f5;">
			  <?php
				  foreach ($subcats as $sc) {
					?>
					<option value="<?php echo $sc->term_id; ?>"><?php echo $sc->name; ?></option>
					<?php
				  }
				  ?>
			  </select>
			  <?php
				$ret = ob_get_clean();
				$ret = array('st' => 'ok','data'=> $ret ,'msg'=>'found');
				echo json_encode($ret);
			}
			exit;
			
		}	
		
		function front_end(){
		?>
			<script>
				jQuery('#aspk_buy_now_btn').html('BUY ME NOW');
			</script>
		<?php
			wp_enqueue_script('jquery');
			wp_enqueue_style( 'style', plugins_url('css/treas.css', __FILE__) );
			wp_enqueue_script('bootstrap-js', plugins_url('js/agile-bootstrap.js', __FILE__));
			wp_enqueue_script('jquery-ui-widget', '', array('jquery','jquery-ui-core'));
			wp_enqueue_script('jquery.iframe-transport', plugins_url('js/jquery.iframe-transport.js', __FILE__) );
			wp_enqueue_script('jquery-fileupload-treas', plugins_url('js/jquery.fileupload.js', __FILE__) );
		}
		
		function install(){
		
		
		
		
		}
		
		function wp_enqueue_scripts(){
		
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-tooltip',array( 'jquery','jquery-ui-core' ));
			
		}

		function handle_image_upload(){
			require 'UploadHandler.php';
			
			global $current_user;
			get_currentuserinfo();
			$current_user_id=$current_user->ID;
			if(!isset($current_user_id) || $current_user_id=='')
				$current_user_id='guest';
			$upload_handler = new UploadHandler(null,$current_user_id,true,null);
			exit(); 
		}
		
		function aspk_get_catogries(){
			
			$prod_cat_args = array(
			  'taxonomy'     => 'product_cat', //woocommerce
			  'orderby'      => 'name',
			  'empty'        => 0
			);
			$categories =  get_categories( $prod_cat_args );
			alog('$categories',$categories,__FILE__,__LINE__);
			return $categories;
		
		}
		
		function add_term_relationship($tid,$post_id){
			global $wpdb;
			
			$term = 0;
			$sql = "insert into {$wpdb->prefix}term_relationships ( object_id, term_taxonomy_id,term_order) values({$post_id} , {$tid} , {$term} )";
			alog('$sql',$sql,__FILE__,__LINE__);
			$wpdb->query($sql);
		}
		
		function add_post_desc($post_id, $post_desc){
		
			$my_post = array(
				  'ID'           => $post_id,
				  //'post_excerpt' => $post_desc
				  'post_content' => $post_desc
			  );

			wp_update_post($my_post);
		}
		
		function create_user(){
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
			$random_password = substr( str_shuffle( $chars ), 0, $length );
			$email = explode('@' , $_POST['e_mail']);
			$user_id = wp_create_user( $email[0], $random_password, $_POST['e_mail'] );
			if(! is_numeric($user_id)){
				$user = get_user_by('email' ,$_POST['e_mail']);
				$user_id = $user->ID;
			}
			return $user_id;
		}
		
		function create_product($user_id,$select_category,$select_sub_cat,$item_head,$asking_price,$retail_price,$reserve_price,$txt_area,$width,$depth,$height,$condition,$country,$city){
			
			$post = array(
				'post_author' => $user_id,
				'post_content' => '',
				'post_status' => "draft",
				'post_title' => $item_head,
				'post_parent' => '',
				'post_type' => "product",
			);
			
			$post_id = wp_insert_post( $post );
			$post_thumbnail_id = get_post_thumbnail_id( $post_id );
			
			if($post_id){
				$this->add_term_relationship($select_category,$post_id);
				update_post_meta( $post_id, '_select_category', $select_category );
				update_post_meta( $post_id, '_select_sub_category', $select_sub_cat );
				update_post_meta( $post_id, '_purchase_note', $txt_area );	
				$this->add_post_desc($post_id, $txt_area);
				update_post_meta( $post_id, '_weight', $width);			
				update_post_meta( $post_id, '_length', $depth );			
				update_post_meta( $post_id, '_height', $height);			
				update_post_meta( $post_id, '_regular_price', $asking_price);
				update_post_meta( $post_id, '_sale_price', $retail_price);			
				update_post_meta( $post_id, '_aspk_price_reserve', $reserve_price );
				update_post_meta( $post_id, '_aspk_condition', $condition );
				update_post_meta( $post_id, '_aspk_country', $country );
				update_post_meta( $post_id, '_aspk_city', $city );
			
			}
		}
		
		function random_password( $length = 8 ) { //generates random password.
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;?";
			$password = substr( str_shuffle( $chars ), 0, $length );
			return $password;
		}
		
		function seller_form(){
			
			$user_id = get_current_user_id();
			$category =  $this->aspk_get_catogries(); 		
			if(isset($_POST['aspk_submit'])){
				$select_category = $_POST['select_category'];
					$item_head = $_POST['item_head'];
					$asking_price = $_POST['asking_price'];
					$retail_price = $_POST['retail_price'];
					$reserve_price = $_POST['reserve_price'];
					$txt_area = $_POST['txt_area'];
					$condition    = $_POST['radio'];
					$width = $_POST['width'];
					$depth  = $_POST['depth'];
					$height = $_POST['height'];
					$username = $_POST['my_name'];
					$street_adress = $_POST['street_adress'];
					$city = $_POST['city'];
					$countries = $_POST['countries'];
					$zip_code = $_POST['zip_code'];
					$phone_number = $_POST['phone_number'];
					$radio_1 = $_POST['select_me'];
				if(! $user_id) $user_id = $this->create_user();
				if(is_numeric($user_id)){
					update_user_meta($user_id, 'first_name',$username);
					update_user_meta($user_id, 'city',$city);
					update_user_meta($user_id, 'zip_code',$zip_code);
					update_user_meta($user_id, 'phone_number',$phone_number);
					update_user_meta($user_id, 'country',$countries);
				}	
				 $this->create_product($user_id,$select_category,$select_sub_cat,$item_head,$asking_price,$retail_price,$reserve_price,$txt_area,$width,$depth,$height,$condition,$countries,$city);
				
				
			}
			 
	?>		
		<div class="tw-bs container">
			<form method="post" action="" id="aspk_seller_form">
				<div class="row aspk_contact_us" style="clear: left;width: 43em;margin-left: 0.1em;margin-bottom: 1em;border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5">
					<div class="col-md-2"><img style="height: 4em;" src="<?php echo plugins_url('images/form-image.png', __FILE__);?>"></div>
					<div class="col-md-7" style="float:left;margin-top:1em">Are you a professional seller? </div>

				</div>
				<div class="row aspk_Select_Category">
					<div class="col-md-12">Select Category *</div>
				</div>
				<div class="row">
					<div class="col-md-11 aspk_text_feild_select_category">
					
						<Select  class = "form-control" id="main_categories" name="select_category" onchange="get_sub_categories();"  style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5" >
									<?php foreach($category as $cat) { ?>
										<option value = "<?php echo $cat->term_id; ?>" ><?php echo $cat->name; ?></option>
							<?php } ?>
											
						</select>
					</div>
				</div>	
				<div id="aspk_respone">
					<div class="row aspk_Select_Category" id="show_sub_cat" style="margin-bottom: 1em;display:none;">
						<div class="col-md-12 sub_category" style="margin-top: 1em;">Sub Category *</div>
					</div>
					<div class="row" style="margin-bottom: 1em;">
						<div class="col-md-11 aspk_text_feild_select_category_sub">
							<div id="aspk_sub_cat">
							</div>
						</div>
					</div>				
				</div>				
				<div class="row aspk_title" style="margin-top: 1em;">
					<div class="col-md-12" >Title *</div>
				</div>
				<div class="row">
					<div class="col-md-12 aspk_text_feild_title">
						<input  class = "form-control" type="text" name="item_head" style="height:2em; width:50em;border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5"  value="" required/>
					</div>
				</div>	
				<div class="row" style="clear:left;">
					
					<div class="col-md-4" id="aspk_asking_price">Asking Price * <div class="asking"></div></div>
					<div class="col-md-3" id="aspk_retail_price">Est. Retail Price <img id="img_1" title="Estimated Retail Price is what you believe the listing will sell for new or in the case of discontinued or vintage items, what you believe the market value is for this item." style="height: 1em;" src="<?php echo plugins_url('images/question_mark.png', __FILE__);?>"></div>
					<div class="col-md-4" style="padding-left:4em">Reserve Price<img id="img_2" title="Set a minimum price you're willing to accept for this item. All offers above this price will be automatically accepted." style="height: 1em;" src="<?php echo plugins_url('images/question_mark.png', __FILE__);?>"> </div>
		
				</div>
				<div class="row" style="clear:left;margin-top: 1em; width:46.5em;">
					<div class="col-md-4">
						<span style="float:left;margin-top: 0.4em;">$</span>
							<div style="float:left;">
								<input class="form-control" id="asking_price_1" type="number" name="asking_price" style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5"required/>
							</div>
					</div>	
					<div class="col-md-4">
						<span style="float:left;margin-top: 0.4em;">$</span>
							<div style="float:left;">
								<input class="form-control" id="retail_price" type="number" name="retail_price" style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5" required/>
							</div>
					</div>
					<div class="col-md-4">
						<span style="float:left;margin-top: 0.4em;">$</span>
							<div style="float:left;">
								<input class="form-control" id="reserve_price" type="number" name="reserve_price" style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5" required/>
							</div>
					</div>
				</div>
				<div class="row" id="msg" style="clear:left;display:none;color:red;margin-left: 12em;">
					<div class="col-md-12" >Asking price must be greater than retail price </div>
				</div>
				
				<div class="row aspk_description">
					<div class="col-md-12" style="margin-top:1em">Description *</div>
				</div>
				<div class="row">
					<div class="col-md-12 aspk_text_feild_select_category">
						<textarea class="form-control" name="txt_area" style="width:51em; height:20em;border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5" required/></textarea>
					</div>
				</div>	
				<div class="row" style="margin-top:1em">
					<div class="col-md-12" style = "margin-bottom: 1em;">Condition *</div>
				</div>
				<div class="row" style = "clear:left;">
					<div class="col-md-12">
						<span>
							<input type="radio" name="radio" value ="Excellent - Minor wear consistent with age and history."  name="radio_field"  >
						</span>
						Excellent - Minor wear consistent with age and history.
					</div>	
				</div>
				<div class="row">
					<div class="col-md-12" >
						<span>
							<input type="radio" name="radio" value ="Good - Moderate wear and tear, but still has good years left."  name="radio_field_1">
						</span>
						Good - Moderate wear and tear, but still has good years left.
					</div>	
				</div>
				<div class="row">
					<div class="col-md-12" >
						<span>
							<input type="radio" name="radio" value ="Fair - Has lived a full life and has a distressed look with noticeable wear."  name="radio_field_2" >
						</span>
						Fair - Has lived a "full life" and has a distressed look with noticeable wear.
					</div>	
				</div>
				<div class="row" style = "clear:left;">
					<div class="col-md-12">
						<span>
							<input type="radio" name="radio" value ="Needs Work - Great bones, but will need some work.."  name="radio_field"  >
						</span>
						Needs Work - Great bones, but will need some work.
					</div>	
				</div>
				<div class="row aspk_dimensions" style="margin-top:1em">
					<div class="col-md-12">Dimensions (Inches) *</div>
				</div>
				<div class="row" style="clear:left;margin-top: 1em;">
					<div class="col-md-4">
						<span style="float:left;margin-top: 0.4em;">W</span>
							<div style="float:left;">
								<input class="form-control" type="number" name="width" placeholder="Width" style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5" required/>
							</div>
					</div>	
					<div class="col-md-4">
						<span style="float:left;margin-top: 0.4em;">D</span>
							<div style="float:left;">
								<input class="form-control" type="number" name="depth" placeholder="Depth" style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5" required/>
							</div>
					</div>
					<div class="col-md-4">
						<span style="float:left;margin-top: 0.4em;">H</span>
							<div style="float:left;">
								<input class="form-control" type="number" name="height" placeholder="Height" style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5"required/>
							</div>
					</div>
				</div>
				<div class="row aspk_pickup_adress">
					<div class="col-md-12"style="margin-top:1em">Pickup Address</div>
				</div>
				<div class="row">
					<div class="col-md-12 aspk_pickupadreess">
						<input  class = "form-control" type="text" name="my_name" placeholder="Name" style="width:52em;border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5" value="" required/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 aspk_streetadress">
						<input  class = "form-control" type="text" name="street_adress" placeholder="Street address" style="width:52em;border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5"  value="" required/>
					</div>
				</div>
				<?php if(! $user_id){ ?>
					<div class="row">
						<div class="col-md-12 aspk_streetadress">
							<input  class = "form-control" type="email" name="e_mail" placeholder="E-Mail" style="width:52em;border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5"  value="" required/>
						</div>
					</div>
				<?php } ?>
				<div class="row" style="clear:left;">
					<div class="col-md-2 aspk_city">
						<input  class = "form-control" type="text" name="city" placeholder="City"  value="" style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5" required/>
					</div>
						<div class="col-md-2" >
							<Select  class = "form-control" name="countries" style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5">
										<option value="Alabama">Alabama</option>
											<option value="Alaska">Alaska</option>
											<option value="American Samoa">American Samoa</option>
											<option value="Arizona">Arizona</option>
											<option value="Arkansas">Arkansas</option>
											<option value="California">California</option>
											<option value="Colorado">Colorado</option>
											<option value="Connecticut">Connecticut</option>
											<option value="Delaware">Delaware</option>
											<option value="District of Columbia">District of Columbia</option>
											<option value="Florida">Florida</option>
											<option value="Georgia">Georgia</option>
											<option value="Guam">Guam</option>
											<option value="Hawaii">Hawaii</option>
											<option value="Idaho">Idaho</option>
											<option value="Illinois">Illinois</option>
											<option value="Indiana">Indiana</option>
											<option value="Iowa">Iowa</option>
											<option value="Kansas">Kansas</option>
											<option value="Kentucky">Kentucky</option>
											<option value="Louisiana">Louisiana</option>
											<option value="Maine">Maine</option>
											<option value="Maryland">Maryland</option>
											<option value="Massachusetts">Massachusetts</option>
											<option value="Michigan">Michigan</option>
											<option value="Minnesota">Minnesota</option>
											<option value="Mississippi">Mississippi</option>
											<option value="Missouri">Missouri</option>
											<option value="Montana">Montana</option>
											<option value="Nebraska">Nebraska</option>
											<option value="Nevada">Nevada</option>
											<option value="New Hampshire">New Hampshire</option>
											<option value="New Jersey">New Jersey</option>
											<option value="New Mexico">New Mexico</option>
											<option value="New York">New York</option>
											<option value="North Carolina">North Carolina</option>
											<option value="North Dakota">North Dakota</option>
											<option value="Northern Marianas Islands">Northern Marianas Islands</option>
											<option value="Ohio">Ohio</option>
											<option value="Oklahoma">Oklahoma</option>
											<option value="Oregon">Oregon</option>
											<option value="Pennsylvania">Pennsylvania</option>
											<option value="Puerto Rico">Puerto Rico</option>
											<option value="Rhode Island">Rhode Island</option>
											<option value="South Carolina">South Carolina</option>
											<option value="South Dakota">South Dakota</option>
											<option value="Tennessee">Tennessee</option>
											<option value="Texas">Texas</option>
											<option value="Utah">Utah</option>
											<option value="Vermont">Vermont</option>
											<option value="Virginia">Virginia</option>
											<option value="Virgin Islands">Virgin Islands</option>
											<option value="Washington">Washington</option>
											<option value="West Virginia">West Virginia</option>
											<option value="Wisconsin">Wisconsin</option>
											<option value="Wyoming">Wyoming</option>
							</select>
						</div>	
					<div class="col-md-3">
						<input  class = "form-control" id="aspk_zip_code" type="text" name="zip_code" placeholder="ZIP Code"  value="" style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5" required/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 aspk_streetadress">
						<input  class = "form-control" type="number" name="phone_number" placeholder="Phone Number"  value="" style="width:52em;border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5" required/>
					</div>
				</div>
				<div style="border: 1px solid #e3e3e3;border-radius: 3px;background-color: #f5f5f5;width:47em;padding:1em;">
					<div class="row">
						<div class="col-md-12" >
							<span>
								<input type="checkbox"   name="select_me">
							</span>
							Allow local buyers to pick up in person
						</div>	
					</div>
					<div class="row" style="clear:left">
						<div class="col-md-12" >
							Let local buyers pick up the item from you directly. This results in substantial savings for the buyer and 
							may help you sell your item faster.
						</div>	
					</div>
					<div class="row" style="margin-top:1em">
						<div class="col-md-12" >
							<span>
								<input type="checkbox"   name="select_me">
							</span>
							Offer free shipping on this item
						</div>	
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-12">
							Sell your listings more quickly! Attract buyers by offering free shipping on your items. Please Note: By 
							offering free shipping, you will be responsible for shipping your sold listing. This includes selecting the 
							appropriate shipping partner (UPS, FedEx, etc.) and any associated shipping costs and risks.
						</div>	
					</div>
				</div>
				<div class="row" style="clear:left; margin-top:1em">
					<div class="col-md-12">
						<input  type="submit" onclick="myFunction()" name="aspk_submit"  value="next"/>
					</div>
				</div>
			</form>	
		</div>	
			
			<script>
				function get_sub_categories(){
					
					var main_categories = jQuery('#main_categories').val();
					
					var data = {
								'action': 'show_sub_category',
								'main_categories': main_categories,						// We pass php values differently!
								
					};
							var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
							jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							
							if(obj.st == 'error'){
								jQuery('#aspk_respone').hide();
								
							}
							if(obj.st == 'ok'){
								jQuery('#aspk_sub_cat').html(obj.data);
								jQuery('#aspk_respone').show();
								
							}
							 });
						
					
				}
			</script>
			<script>
				jQuery( document ).ready(function() {
					jQuery( '#img_1' ).tooltip();
					jQuery( '#img_2' ).tooltip();
				});
			</script>
			
			<script>
				jQuery(document).ready(function() {
					jQuery('#aspk_zip_code').formValidation({
						fields: {
							postcode: {
								validators: {
									regexp: {
										regexp: /^\d{5}$/,
										message: 'The US zipcode must contain 5 digits'
									}
								}
							}
						}
					});
				});
			</script>
			
			<script>
				jQuery( "#aspk_seller_form" ).submit(function( event ) {
				    var est_price = jQuery('#asking_price_1').val();
					var res_price = jQuery('#retail_price').val();
						if(res_price < est_price){
							jQuery("#msg").show();
							return false;
					}
					
				});
			</script>
			
			<?php			
			$this->upload_photo();	
		}
		
		function upload_photo(){
			?>
				<div class="tw-bs container">
					<form method="post" action="">
						<div class="upload_image">
							<div class="row" style="clear:left">
								<div class="col-md-12" style="text-align:center; ">Drag and Drop Your Photo Here</div>
							</div>
							<div class="row" style="clear:left" >
								<div class="col-md-12" style="">
									<div id="progress1" class="progress" style="display:none;margin-bottom: 5px;">
										<div class="progress-bar progress-bar-success" style="background-color:#BAD560;" ></div>
									</div>
									<div id="upload-btn1" class="upload_image"><label class="upload_label"><input id="fileupload1" type="file" name="files[]" />UPLOAD PHOTO 1</label></div>
								</div>
							</div>
						</div>
					</form>
				</div>	
				<script>
					jQuery( "#_sub_category" ).change(function() {
					alert( "Handler for .change() called." );
					});
				</script>
			

				<script>
					var url = "<?php echo admin_url('admin-ajax.php?action=aspk_jq_uploader'); ?>";
					
					jQuery('#fileupload1').fileupload({
					url: url,
					dataType: 'json',
					send: function (e, data) {
						jQuery('#progress1').show();
					},
					done: function (e, data) {
						jQuery.each(data.result.files, function (index, file) {
							jQuery('#upload-btn1').hide();
							jQuery('#img_controls1').show();
							jQuery('#img1').attr('src', file.url);
							var img_url1 =jQuery('#upload_image1').attr('src', file.url);

						});
					},
					progressall: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
						jQuery('#progress1 .progress-bar').css('width',progress + '%');
					}
				}).prop('disabled', !jQuery.support.fileInput).parent().addClass(jQuery.support.fileInput ? undefined : 'disabled');
				
				</script>
			<?php
			
		}
		
	}//class bracket	
}//condition bracket	
new Agile_treas();